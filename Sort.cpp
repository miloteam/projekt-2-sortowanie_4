#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

using namespace std;

class film{                                        
    public:
    string movie;
    int rating;
};


int *pomocnicza;

void shell(film* tab,int ilosc){

    tab[ilosc];
    int odstep,indeks,element;

    
    odstep=1;
    do{

        odstep=3*odstep+1;

    }while(odstep<ilosc);

        odstep/=9;
    if(odstep==0){
        
        odstep++; 
    }
  
    
   do
  {
    for(int i=ilosc-odstep-1; i>= 0; i--)
    {
      element = tab[i].rating;
      indeks = i+odstep;
      while((indeks<ilosc) && (element>tab[indeks].rating))
      {
        tab[indeks-odstep].rating = tab[indeks].rating;
        indeks+=odstep;
      }
      tab[indeks-odstep].rating=element;
    }
    odstep/=3;
  }while(odstep > 0);

}
void quick_sort(film * tab,int end, int start){

float axis = tab[(start+end)/2].rating;
int l,p;
int hold;
p = end;
l = start;           
     do
     {
        while(tab[p].rating> axis){
            p--;
        }
        while(tab[l].rating < axis){
            l++;
        }
    if( l <= p ){
        hold = tab[p].rating;
        tab[p].rating = tab[l].rating;
        tab[l].rating = hold;
    p--;
    l++;
    }

    } while (l <= p);

    if(p>start) quick_sort(tab,p,start);
    
    if(l<end) quick_sort(tab,end,l);
}
void pdf_scalanie(film *tab,int s, int m,int e){

    int l,p;
   
    l = s;
    p = m+1;

        for(int i = l; i <= e; i++){
            pomocnicza[i] = tab[i].rating;
        }
        
        int j = l;
        while(j <= e){
                if(l<=m)
                if(p <= e)
                if(pomocnicza[p] < pomocnicza[l])
                tab[j].rating = pomocnicza[p++];
                else
                tab[j].rating = pomocnicza[l++];
                else
                tab[j].rating = pomocnicza[l++];
                else
                tab[j].rating = pomocnicza[p++];
                
            j++;
    
}
}
void scalanie(film *tab,int s, int e){
    
    if(e<=s){
        return; 
    }
	int axis = (s+e)/2;
	
	scalanie(tab, s, axis); 
	scalanie(tab, axis+1, e);
	pdf_scalanie(tab, s, axis, e);  
} 
void load(film * tab,int ile){

    ifstream plik("projekt2_dane.csv");

    string pomoc;
    string pomoc2;
    string pomoc4;
    int pomoc3;                             
        getline(plik,pomoc);                    //pominięcie pierwszej linini
        int i=0;
       
    while(i<ile && !plik.eof()){                //While wykonuje się póki 'i' jest mniejsze od ilosci elementow w tablicy lub do końca pliku
                                                //W pliku znajduje się mniej niż milion poprawnych elementow, a wiec zalozylem ze sortowanie dla miliona 
                                                //było sortowaniem dla maksymalnej ilosci dany z plikow 
    getline(plik, pomoc);                       //Bierzemy jedna linijke 
    int start = pomoc.find(',')+1;              //Znajdz pierwszy przecinek
    int end = pomoc.rfind(',');                 //Znajdz ostatni przecinek
    pomoc2 = pomoc.substr(start, end-start);    //Branie elementu pomiedzy anzlezionymi przecinkami - tytul
    pomoc4 = pomoc.substr(end+1,2);             //Branie 2 znakow po ostatnim przecinku - ranking
    if(pomoc4.empty()){                         //Pomijanie danych nie posiadajacych znakow po ostatnim przecinku 
        
        continue;
    }  
    pomoc3=stoi(pomoc4);                         //konwersja stringa na inta

       tab[i].movie = pomoc2;                    //wpisanie do tablicy wartosci    
       tab[i].rating = pomoc3;
       i++;
    }           
    
}
float srednia(film * tab,int r){
        
        float suma = 0;

        for(int i=0;i<r;i++){

            suma+=tab[i].rating;

        }
        
    return suma/r;

}
float mediana(film * tab,int r){

    int n = r/2;
    if(r%2==0) return (tab[n].rating + tab[n+1].rating)/2.0;
    else return tab[n].rating;


}

int main(){
   

     film *tab1 = new film[10000];
     pomocnicza = new int[10000];

     film *tab2 = new film[100000];
     pomocnicza = new int[100000];

     film *tab3 = new film[500000];
     pomocnicza = new int[500000];

     film *tab4 = new film[1000000];
     pomocnicza = new int[1000000];

    cout << "================================ filtrowanie pliku" << endl;

    auto start = std::chrono::steady_clock::now();
    load(tab1,10000);
    auto end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;
    

    start = std::chrono::steady_clock::now();
    load(tab2,100000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;
   

    start = std::chrono::steady_clock::now();
    load(tab3,500000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;
   

    start = std::chrono::steady_clock::now();
    load(tab4,1000000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

    cout << "================================ Sortowanie typu quicksort" << endl;

    start = std::chrono::steady_clock::now();
    quick_sort(tab1,10000-1,0);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;
    

    start = std::chrono::steady_clock::now();
    quick_sort(tab2,100000-1,0);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;


    start = std::chrono::steady_clock::now();
    quick_sort(tab3,500000-1,0);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;


    start = std::chrono::steady_clock::now();
    quick_sort(tab4,1000000-1,0);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;


    cout << "================================ Sortowanie Shell" << endl;
    load(tab1,10000);
    load(tab2,100000);
    load(tab3,500000);
    load(tab4,1000000);

    start = std::chrono::steady_clock::now();
    shell(tab1,10000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

     start = std::chrono::steady_clock::now();
    shell(tab2,100000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

     start = std::chrono::steady_clock::now();
    shell(tab3,500000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

     start = std::chrono::steady_clock::now();
    shell(tab4,1000000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

    
    cout << "================================ Sortowanie przez scalanie" << endl;
    
    
    load(tab1,10000);
    load(tab2,100000);
    load(tab3,500000);
    load(tab4,1000000);
    

    start = std::chrono::steady_clock::now();
    scalanie(tab1,0,10000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

    start = std::chrono::steady_clock::now();
    scalanie(tab2,0,100000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

    start = std::chrono::steady_clock::now();
    scalanie(tab3,0,500000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

    start = std::chrono::steady_clock::now();
    scalanie(tab4,0,1000000);
    end = std::chrono::steady_clock::now();
    cout << chrono::duration_cast<std::chrono::microseconds>(end-start).count() << endl;

   

    cout << "================================ Srednia wartosc dla 10000 posortowanych danych" << endl;
    cout << srednia(tab1,10000) << endl;
    cout << "================================ Srednia wartosc dla 100000 posortowanych danych" << endl;
    cout << srednia(tab2,100000) << endl;
    cout << "================================ Srednia wartosc dla 500000 posortowanych danych" << endl;
    cout << srednia(tab3,500000) << endl;
    cout << "================================ Srednia wartosc dla 1000000 posortowanych danych" << endl;
    cout << srednia(tab4,1000000) << endl;
    cout << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << endl;
    cout << "================================ Mediana dla 10000 posortowanych danych" << endl;
    cout << mediana(tab1,10000) << endl;
    cout << "================================ Mediana dla 100000 posortowanych danych" << endl;
    cout << mediana(tab2,100000) << endl;
    cout << "================================ Mediana dla 500000 posortowanych danych" << endl;
    cout << mediana(tab3,500000) << endl;
    cout << "================================ Mediana wartosc dla 1000000 posortowanych danych" << endl;
    cout << mediana(tab4,1000000) << endl;
    

    delete [] tab1;
    delete [] tab2;
    delete [] tab3;
    delete [] tab4;
}





